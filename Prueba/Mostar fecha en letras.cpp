#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void ConversionMes(char [],int);
void ConversionDia(char dia[],int);
void Creditos();

struct FECHA{
	int diasem;
	int dia;
	int mes;
	int anio;
};
struct FECHA_LETRAS{
	char diasemLetras[10];
	//char diaLetras[30];
	char mesLetras[15];
	//char anioLetras[50];
};
 
int main(){
	struct FECHA fecha;
	struct FECHA_LETRAS fechaConv;
	
	printf("====================== DATOS DE NACIMIENTO ======================\n");
	printf("Ingrese el numero correspondiente al dia de la semana:");
	printf("\n 1. Domingo\n 2. Lunes\n 3. Martes\n 4. Miercoles\n 5. Jueves\n 6. Viernes\n 7. Sabado\n");
	printf("Ingrese su opcion: [ ]\b\b");
	scanf("%d",&fecha.diasem);
	printf("Ingrese el dia:    [  ]\b\b\b");
	scanf("%d",&fecha.dia);
	printf("Ingrese el mes:    [  ]\b\b\b");
	scanf("%d",&fecha.mes);
	printf("Ingrese el anio:   [    ]\b\b\b\b\b");
	scanf("%d",&fecha.anio);
 	
	printf("\nLa fecha de nacimiento es:   %02d / %02d / %04d \n",
			fecha.dia,fecha.mes,fecha.anio);
	
	ConversionMes(fechaConv.mesLetras,fecha.mes);
	ConversionDia(fechaConv.diasemLetras,fecha.diasem);
	
	printf("\nLa fecha de naciminto es: %s %d de %s de %d\n",
			fechaConv.diasemLetras,fecha.dia,fechaConv.mesLetras,fecha.anio);
	
	fflush(stdin);
	getchar();
	Creditos();
	fflush(stdin);
	getchar();
 	
	return 0;
}

void ConversionMes(char mes[],int meses){
	switch(meses){
 		case 1:  strcpy(mes, "Enero");      break;
 		case 2:  strcpy(mes, "Febrero");    break;
 		case 3:  strcpy(mes, "Marzo");      break;
 		case 4:  strcpy(mes, "Abril");      break;
 		case 5:  strcpy(mes, "Mayo");       break;
 		case 6:  strcpy(mes, "Junio");      break;
 		case 7:  strcpy(mes, "Julio");      break;
 		case 8:  strcpy(mes, "Agosto");     break;
 		case 9:  strcpy(mes, "Septiembre"); break;
 		case 10: strcpy(mes, "Octubre");    break;
 		case 11: strcpy(mes, "Noviembre");  break;
 		case 12: strcpy(mes, "Diciembre");  break;
	}
}

void ConversionDia(char dia[],int dias){
	switch(dias){
		case 1:  strcpy(dia,"Domingo");     break;
		case 2:  strcpy(dia,"Lunes");       break;
		case 3:  strcpy(dia,"Martes");      break;
		case 4:  strcpy(dia,"Miercoles");   break;
		case 5:  strcpy(dia,"Jueves");      break;
		case 6:  strcpy(dia,"Viernes");     break;
		case 7:  strcpy(dia,"Sabado");      break;
	}
}

void Creditos(){
	printf("\nPrograma realizado por: Victor Maquilon Chacha");
	printf("\nCurso:                  ISI-S-MA-2-3");
}
 
 
