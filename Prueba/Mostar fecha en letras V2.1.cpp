#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct FECHA{
	int diasem;
	int dia;
	int mes;
	int anio;
};
struct FECHA_LETRAS{
	char diasemLetras[10];
	char diaLetras[50];
	char mesLetras[15];
	char anioLetras[70];
};

void Menu(FECHA);
void ConversionDia(char [],int);
void ConversionMes(char [],int);
void ConversionAnio(char [],int);

 int main(){
	struct FECHA fecha;
	struct FECHA_LETRAS fechaConv;
	
	Menu(fecha);
 	
	printf("\nLa fecha de nacimiento es:   %02d / %02d / %04d \n",
			fecha.dia,fecha.mes,fecha.anio);
	
	ConversionMes(fechaConv.mesLetras,fecha.mes);
	ConversionDia(fechaConv.diasemLetras,fecha.diasem);
	ConversionAnio(fechaConv.anioLetras,fecha.anio);
	ConversionAnio(fechaConv.diaLetras,fecha.dia);
	
	printf("\nLa fecha de naciminto es: %s %d de %s de %d\n\n",
			fechaConv.diasemLetras,
			fecha.dia,
			fechaConv.mesLetras,
			fecha.anio);
	
	printf("\n %s %s de %s de %s",
		fechaConv.diasemLetras,
		fechaConv.diaLetras,
		fechaConv.mesLetras,
		fechaConv.anioLetras);
	
	system("pause");
 	
	return 0;
}

void Menu(FECHA fecha){
	printf("====================== DATOS DE NACIMIENTO ======================\n");
	printf("Ingrese el numero correspondiente al dia de la semana:");
	printf("\n 1. Domingo\n 2. Lunes\n 3. Martes\n 4. Miercoles\n 5. Jueves\n 6. Viernes\n 7. Sabado\n");
	printf("Ingrese su opcion: [ ]\b\b");
	scanf("%d",&fecha.diasem);
	printf("Ingrese el dia:    [  ]\b\b\b");
	scanf("%d",&fecha.dia);
	printf("Ingrese el mes:    [  ]\b\b\b");
	scanf("%d",&fecha.mes);
	printf("Ingrese el anio:   [    ]\b\b\b\b\b");
	scanf("%d",&fecha.anio);
}

void ConversionMes(char mes[],int meses){
	switch(meses){
 		case 1:  strcpy(mes, "Enero");      break;
 		case 2:  strcpy(mes, "Febrero");    break;
 		case 3:  strcpy(mes, "Marzo");      break;
 		case 4:  strcpy(mes, "Abril");      break;
 		case 5:  strcpy(mes, "Mayo");       break;
 		case 6:  strcpy(mes, "Junio");      break;
 		case 7:  strcpy(mes, "Julio");      break;
 		case 8:  strcpy(mes, "Agosto");     break;
 		case 9:  strcpy(mes, "Septiembre"); break;
 		case 10: strcpy(mes, "Octubre");    break;
 		case 11: strcpy(mes, "Noviembre");  break;
 		case 12: strcpy(mes, "Diciembre");  break;
	}
}

void ConversionDia(char dia[],int dias){
	switch(dias){
		case 1:  strcpy(dia,"Domingo");     break;
		case 2:  strcpy(dia,"Lunes");       break;
		case 3:  strcpy(dia,"Martes");      break;
		case 4:  strcpy(dia,"Miercoles");   break;
		case 5:  strcpy(dia,"Jueves");      break;
		case 6:  strcpy(dia,"Viernes");     break;
		case 7:  strcpy(dia,"Sabado");      break;
	}
}
 
void ConversionAnio(char num[],int numero){
	int n1=0,n2=0,n3=0,n4=0;
	char s1[]="",s2[]="",s3[]="",s4[]="";
	n1 = ((numero/1000)%10); //Unidades de mil
	n2 = ((numero/100)%10);  //Centenas
	n3 = ((numero/10)%10);   //Decenas
	n4 = ((numero/1)%10);    //Unidades
	
	if(n1!=0){
		switch(n1){
			case 1: strcpy(s1, "Mil");        break;
			case 2: strcpy(s1, "Dos Mil");    break;
			case 3: strcpy(s1, "Tres Mil");   break;
			case 4: strcpy(s1, "Cuatro Mil"); break;
			case 5: strcpy(s1, "Cinco Mil");  break;
			case 6: strcpy(s1, "Seis Mil");   break;
			case 7: strcpy(s1, "Siete Mil");  break;
			case 8: strcpy(s1, "Ocho Mil");   break;
			case 9: strcpy(s1, "Nueve Mil");  break;
		}
	}
	if(n2!=0){
		switch(n2){
			case 1: (n3==0&&n4==0)?strcpy(s2, "Cien"):
					strcpy(s2, "Ciento");        break;
			case 2: strcpy(s2, "Doscientos");    break;
			case 3: strcpy(s2, "Trescientos");   break;
			case 4: strcpy(s2, "Cuatroscientos");break;
			case 5: strcpy(s2, "Quinientos");    break;
			case 6: strcpy(s2, "Seiscientos");   break;
			case 7: strcpy(s2, "Setescientos");  break;
			case 8: strcpy(s2, "Ochoscientos");  break;
			case 9: strcpy(s2, "Novescientos");  break;
		}
	}
	if(n3!=0){
		switch(n3){
			case 1: if(n4==0){
						strcpy(s3, "Diez");
					}else{
						if(n4>5){
							strcpy(s3, "Dieci");
						}    
					}                                                             break;
			case 2: (n4==0)?strcpy(s3, "Veinte")    :strcpy(s3, "Veinti");        break;
			case 3: (n4==0)?strcpy(s3, "Treinta")   :strcpy(s3, "Treinta y ");    break;
			case 4: (n4==0)?strcpy(s3, "Cuarenta")  :strcpy(s3, "Cuarenta y ");   break;
			case 5: (n4==0)?strcpy(s3, "Cincuenta") :strcpy(s3, "Cincuenta y ");  break;
			case 6: (n4==0)?strcpy(s3, "Sesenta")   :strcpy(s3, "Sesenta y ");    break;
			case 7: (n4==0)?strcpy(s3, "Setenta")   :strcpy(s3, "Setenta y ");    break;
			case 8: (n4==0)?strcpy(s3, "Ochenta")   :strcpy(s3, "Ochenta y ");    break;
			case 9: (n4==0)?strcpy(s3, "Noventa")   :strcpy(s3, "Noventa y ");    break;
		}
	}
	if(n4!=0){
		switch(n4){
			case 1: (n3==1)?strcpy(s4, "Once")    :strcpy(s4, "Uno");      break;
			case 2: (n3==1)?strcpy(s4, "Doce")    :strcpy(s4, "Dos");      break;
			case 3: (n3==1)?strcpy(s4, "Trece")   :strcpy(s4, "Tres");     break;
			case 4: (n3==1)?strcpy(s4, "Catorce") :strcpy(s4, "Cuatro");   break;
			case 5: (n3==1)?strcpy(s4, "Quince")  :strcpy(s4, "Cinco");    break;
			case 6: strcpy(s4, "Seis");    break;
			case 7: strcpy(s4, "Siete");   break;
			case 8: strcpy(s4, "Ocho");    break;
			case 9: strcpy(s4, "Nueve");   break;
		}
	}
	
	strcat(s3,s4);
	strcat(s2,s3);
	strcat(s1,s2);
	strcpy(num,s1);
}
 
 
