#include <stdio.h>
#include <string.h>

struct FECHA{
	int diasem;
	int dia;
	int mes;
	int anio;
}fecha;

struct NUMEROS_LETRAS{
	char s1[25];
	char s2[25];
	char s3[25];
	char s4[25];
};

struct FECHA_LETRAS{
	char diasemLetras[15];
	char mesLetras[15];
	struct NUMEROS_LETRAS numeros[2];
}fechaConv;

void Menu();
void ConversionDia();
void ConversionMes();
void ConversionAnio(int,int);
void Presentar();
void Creditos();

int main(){

	Menu();
	
 	ConversionMes();
	ConversionDia();
	ConversionAnio(fecha.dia,0);
	ConversionAnio(fecha.anio,1);
	
	Presentar();
	
	fflush(stdin);
	getchar();
	
	Creditos();
	
	fflush(stdin);
	getchar();
 	
	return 0;
}

void Menu(){
	printf("====================== DATOS DE NACIMIENTO ======================\n");
	printf("Ingrese el numero correspondiente al dia de la semana:");
	printf("\n 1. Domingo\n 2. Lunes\n 3. Martes\n 4. Miercoles\n 5. Jueves\n 6. Viernes\n 7. Sabado\n");
	printf("Ingrese su opcion: [ ]\b\b");
	fflush(stdin);
	scanf("%d",&fecha.diasem);
	printf("Ingrese el dia:    [  ]\b\b\b");
	fflush(stdin);
	scanf("%d",&fecha.dia);
	printf("Ingrese el mes:    [  ]\b\b\b");
	fflush(stdin);
	scanf("%d",&fecha.mes);
	printf("Ingrese el anio:   [    ]\b\b\b\b\b");
	fflush(stdin);
	scanf("%d",&fecha.anio);
}

void Presentar(){
	printf("\nLa fecha de nacimiento es:   %02d / %02d / %04d \n",
			fecha.dia,
			fecha.mes,
			fecha.anio);
	
	printf("\nLa fecha de nacimiento es: %s %d de %s de %d\n",
			fechaConv.diasemLetras,
			fecha.dia,
			fechaConv.mesLetras,
			fecha.anio);
	
	printf("\n%s %s%s de %s de %s%s%s%s\n",
			fechaConv.diasemLetras,
			fechaConv.numeros[0].s3,
			fechaConv.numeros[0].s4,
			fechaConv.mesLetras,
			fechaConv.numeros[1].s1,
			fechaConv.numeros[1].s2,
			fechaConv.numeros[1].s3,
			fechaConv.numeros[1].s4);
}

void ConversionMes(){
	switch(fecha.mes){
 		case 1:  strcpy(fechaConv.mesLetras, "Enero");      break;
 		case 2:  strcpy(fechaConv.mesLetras, "Febrero");    break;
 		case 3:  strcpy(fechaConv.mesLetras, "Marzo");      break;
 		case 4:  strcpy(fechaConv.mesLetras, "Abril");      break;
 		case 5:  strcpy(fechaConv.mesLetras, "Mayo");       break;
 		case 6:  strcpy(fechaConv.mesLetras, "Junio");      break;
 		case 7:  strcpy(fechaConv.mesLetras, "Julio");      break;
 		case 8:  strcpy(fechaConv.mesLetras, "Agosto");     break;
 		case 9:  strcpy(fechaConv.mesLetras, "Septiembre"); break;
 		case 10: strcpy(fechaConv.mesLetras, "Octubre");    break;
 		case 11: strcpy(fechaConv.mesLetras, "Noviembre");  break;
 		case 12: strcpy(fechaConv.mesLetras, "Diciembre");  break;
	}
}

void ConversionDia(){
	switch(fecha.diasem){
		case 1:  strcpy(fechaConv.diasemLetras, "Domingo");     break;
		case 2:  strcpy(fechaConv.diasemLetras, "Lunes");       break;
		case 3:  strcpy(fechaConv.diasemLetras, "Martes");      break;
		case 4:  strcpy(fechaConv.diasemLetras, "Miercoles");   break;
		case 5:  strcpy(fechaConv.diasemLetras, "Jueves");      break;
		case 6:  strcpy(fechaConv.diasemLetras, "Viernes");     break;
		case 7:  strcpy(fechaConv.diasemLetras, "Sabado");      break;
	}
}
 
void ConversionAnio(int numero,int Ident){
	int n1=0,n2=0,n3=0,n4=0;

	n1 = ((numero/1000)%10); //Unidades de mil
	n2 = ((numero/100)%10);  //Centenas
	n3 = ((numero/10)%10);   //Decenas
	n4 = ((numero/1)%10);    //Unidades
	
	if(n1!=0){
		switch(n1){
			case 1: strcpy(fechaConv.numeros[Ident].s1, "Mil ");        break;
			case 2: strcpy(fechaConv.numeros[Ident].s1, "Dos Mil ");    break;
			case 3: strcpy(fechaConv.numeros[Ident].s1, "Tres Mil ");   break;
			case 4: strcpy(fechaConv.numeros[Ident].s1, "Cuatro Mil "); break;
			case 5: strcpy(fechaConv.numeros[Ident].s1, "Cinco Mil ");  break;
			case 6: strcpy(fechaConv.numeros[Ident].s1, "Seis Mil ");   break;
			case 7: strcpy(fechaConv.numeros[Ident].s1, "Siete Mil ");  break;
			case 8: strcpy(fechaConv.numeros[Ident].s1, "Ocho Mil ");   break;
			case 9: strcpy(fechaConv.numeros[Ident].s1, "Nueve Mil ");  break;
		}
	}
	if(n2!=0){
		switch(n2){
			case 1: (n3==0&&n4==0)?strcpy(fechaConv.numeros[Ident].s2, "Cien "):
					strcpy(fechaConv.numeros[Ident].s2, "Ciento ");        break;
			case 2: strcpy(fechaConv.numeros[Ident].s2, "Doscientos ");    break;
			case 3: strcpy(fechaConv.numeros[Ident].s2, "Trescientos ");   break;
			case 4: strcpy(fechaConv.numeros[Ident].s2, "Cuatroscientos ");break;
			case 5: strcpy(fechaConv.numeros[Ident].s2, "Quinientos ");    break;
			case 6: strcpy(fechaConv.numeros[Ident].s2, "Seiscientos ");   break;
			case 7: strcpy(fechaConv.numeros[Ident].s2, "Setescientos ");  break;
			case 8: strcpy(fechaConv.numeros[Ident].s2, "Ochoscientos ");  break;
			case 9: strcpy(fechaConv.numeros[Ident].s2, "Novescientos ");  break;
		}
	}
	if(n3!=0){
		switch(n3){
			case 1: if(n4==0){
						strcpy(fechaConv.numeros[Ident].s3, "Diez ");
					}else{
						if(n4>5){
							strcpy(fechaConv.numeros[Ident].s3, "Dieci");
						}    
					}                                                                                           break;
			case 2: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Veinte ")    :strcpy(fechaConv.numeros[Ident].s3, "Veinti");       break;
			case 3: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Treinta ")   :strcpy(fechaConv.numeros[Ident].s3, "Treinta y ");   break;
			case 4: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Cuarenta ")  :strcpy(fechaConv.numeros[Ident].s3, "Cuarenta y ");  break;
			case 5: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Cincuenta ") :strcpy(fechaConv.numeros[Ident].s3, "Cincuenta y "); break;
			case 6: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Sesenta ")   :strcpy(fechaConv.numeros[Ident].s3, "Sesenta y ");   break;
			case 7: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Setenta ")   :strcpy(fechaConv.numeros[Ident].s3, "Setenta y ");   break;
			case 8: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Ochenta ")   :strcpy(fechaConv.numeros[Ident].s3, "Ochenta y ");   break;
			case 9: (n4==0)?strcpy(fechaConv.numeros[Ident].s3, "Noventa ")   :strcpy(fechaConv.numeros[Ident].s3, "Noventa y ");   break;
		}
	}
	if(n4!=0){
		switch(n4){
			case 1: (n3==1)?strcpy(fechaConv.numeros[Ident].s4, "Once")    :strcpy(fechaConv.numeros[Ident].s4, "Uno");      break;
			case 2: (n3==1)?strcpy(fechaConv.numeros[Ident].s4, "Doce")    :strcpy(fechaConv.numeros[Ident].s4, "Dos");      break;
			case 3: (n3==1)?strcpy(fechaConv.numeros[Ident].s4, "Trece")   :strcpy(fechaConv.numeros[Ident].s4, "Tres");     break;
			case 4: (n3==1)?strcpy(fechaConv.numeros[Ident].s4, "Catorce") :strcpy(fechaConv.numeros[Ident].s4, "Cuatro");   break;
			case 5: (n3==1)?strcpy(fechaConv.numeros[Ident].s4, "Quince")  :strcpy(fechaConv.numeros[Ident].s4, "Cinco");    break;
			case 6: strcpy(fechaConv.numeros[Ident].s4, "Seis");      break;
			case 7: strcpy(fechaConv.numeros[Ident].s4, "Siete");     break;
			case 8: strcpy(fechaConv.numeros[Ident].s4, "Ocho");      break;
			case 9: strcpy(fechaConv.numeros[Ident].s4, "Nueve");     break;
		}
	}
}

void Creditos(){
	printf("\nPrograma realizado por: Victor Maquilon Chacha");
	printf("\nCurso:                  ISI-S-MA-2-3");
}
 
